package com.coffeebreak.schedule.dao;

import com.coffeebreak.schedule.entity.Pair;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: gera
 * Date: 14.04.14
 * Time: 20:24
 * To change this template use File | Settings | File Templates.
 */

public interface PairDao {

    public void savePair(Pair pair);

    public void deletePair(Pair pair);

    public Pair findPairById(Integer id);

    public List<Pair> findAllPairs();
}