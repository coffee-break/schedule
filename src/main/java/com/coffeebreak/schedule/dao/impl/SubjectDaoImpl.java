package com.coffeebreak.schedule.dao.impl;

import com.coffeebreak.schedule.dao.SubjectDao;
import com.coffeebreak.schedule.entity.Subject;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: gera
 * Date: 03.04.14
 * Time: 20:19
 * To change this template use File | Settings | File Templates.
 */

@Repository("subjectDao")
public class SubjectDaoImpl implements SubjectDao {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void saveSubject(Subject subject) {
        getSession().saveOrUpdate(subject);
    }

    @Override
    public void deleteSubject(Subject subject) {
        getSession().delete(subject);
    }

    @Override
    public Subject findSubjectById(Integer id) {
        return (Subject) getSession().get(Subject.class, id);
    }

    @Override
    public List<Subject> findAllSubject() {
        return getSession().createQuery("from Subject").list();
    }
}