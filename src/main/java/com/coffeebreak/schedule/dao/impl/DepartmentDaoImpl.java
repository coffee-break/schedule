package com.coffeebreak.schedule.dao.impl;

import com.coffeebreak.schedule.dao.DepartmentDao;
import com.coffeebreak.schedule.entity.Department;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Arkady
 * @version 1.0, 16.03.14
 */

@Repository("departmentDao")
public class DepartmentDaoImpl implements DepartmentDao {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void saveDepartment(Department department) {
        getSession().saveOrUpdate(department);
    }

    @Override
    public void deleteDepartment(Department department) {
        getSession().delete(department);
    }

    @Override
    public Department findDepartmentById(Integer id) {
        return (Department) getSession().get(Department.class, id);
    }

    @Override
    public List<Department> findAllDepartments() {
        return getSession().createQuery("from Department").list();
    }
}