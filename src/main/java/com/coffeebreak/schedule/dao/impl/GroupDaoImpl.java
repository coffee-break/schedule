package com.coffeebreak.schedule.dao.impl;

import com.coffeebreak.schedule.dao.GroupDao;
import com.coffeebreak.schedule.entity.Group;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: gera
 * Date: 14.04.14
 * Time: 20:38
 * To change this template use File | Settings | File Templates.
 */

@Repository("groupDao")
public class GroupDaoImpl implements GroupDao {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void saveGroup(Group group) {
        getSession().saveOrUpdate(group);
    }

    @Override
    public void deleteGroup(Group group) {
        getSession().delete(group);
    }

    @Override
    public Group findGroupById(Integer id) {
        return (Group) getSession().get(Group.class, id);
    }

    @Override
    public List<Group> findAllGroups() {
        return getSession().createQuery("from Group").list();
    }
}