package com.coffeebreak.schedule.dao.impl;

import com.coffeebreak.schedule.dao.TeacherDao;
import com.coffeebreak.schedule.entity.Teacher;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: gera
 * Date: 17.03.14
 * Time: 22:05
 * To change this template use File | Settings | File Templates.
 */

@Repository("teacherDao")
public class TeacherDaoImpl implements TeacherDao {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void saveTeacher(Teacher teacher) {
        getSession().saveOrUpdate(teacher);
    }

    @Override
    public void deleteTeacher(Teacher teacher) {
        getSession().delete(teacher);
    }

    @Override
    public Teacher findTeacherById(Integer id) {
        return (Teacher) getSession().get(Teacher.class, id);
    }

    @Override
    public List<Teacher> findAllTeachers() {
        return getSession().createQuery("from Teacher").list();
    }
}