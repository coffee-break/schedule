package com.coffeebreak.schedule.dao.impl;

import com.coffeebreak.schedule.dao.PairDao;
import com.coffeebreak.schedule.entity.Pair;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: gera
 * Date: 14.04.14
 * Time: 20:25
 * To change this template use File | Settings | File Templates.
 */

@Repository("pairDao")
public class PairDaoImpl implements PairDao {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void savePair(Pair pair) {
        getSession().saveOrUpdate(pair);
    }

    @Override
    public void deletePair(Pair pair) {
        getSession().delete(pair);
    }

    @Override
    public Pair findPairById(Integer id) {
        return (Pair) getSession().get(Pair.class, id);
    }

    @Override
    public List<Pair> findAllPairs() {
        return getSession().createQuery("from Pair").list();
    }
}