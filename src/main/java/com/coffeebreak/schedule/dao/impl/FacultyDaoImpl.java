package com.coffeebreak.schedule.dao.impl;

import com.coffeebreak.schedule.dao.FacultyDao;
import com.coffeebreak.schedule.entity.Faculty;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Arkady
 * @version 1.0, 13.03.14
 */

@Repository("facultyDao")
public class FacultyDaoImpl implements FacultyDao {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void saveFaculty(Faculty faculty) {
        getSession().saveOrUpdate(faculty);
    }

    @Override
    public void deleteFaculty(Faculty faculty) {
        getSession().delete(faculty);
    }

    @Override
    public Faculty findFacultyById(Integer id) {
        return (Faculty) getSession().get(Faculty.class, id);
    }

    @Override
    public List<Faculty> findAllFaculties() {
        return getSession().createQuery("from Faculty").list();
    }
}