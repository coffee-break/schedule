package com.coffeebreak.schedule.dao;

import com.coffeebreak.schedule.entity.Subject;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: gera
 * Date: 03.04.14
 * Time: 20:16
 * To change this template use File | Settings | File Templates.
 */

public interface SubjectDao {

    public void saveSubject(Subject subject);

    public void deleteSubject(Subject subject);

    public Subject findSubjectById(Integer id);

    public List<Subject> findAllSubject();
}