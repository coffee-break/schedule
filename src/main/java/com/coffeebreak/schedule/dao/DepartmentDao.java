package com.coffeebreak.schedule.dao;

import com.coffeebreak.schedule.entity.Department;

import java.util.List;

/**
 * @author Arkady
 * @version 1.0, 16.03.14
 */

public interface DepartmentDao {

    public void saveDepartment(Department department);

    public void deleteDepartment(Department department);

    public Department findDepartmentById(Integer id);

    public List<Department> findAllDepartments();
}
