package com.coffeebreak.schedule.dao;

import com.coffeebreak.schedule.entity.Faculty;

import java.util.List;

/**
 * @author Arkady
 * @version 1.0, 13.03.14
 */

public interface FacultyDao {
    
    public void saveFaculty(Faculty faculty);

    public void deleteFaculty(Faculty faculty);

    public Faculty findFacultyById(Integer id);

    public List<Faculty> findAllFaculties();
}