package com.coffeebreak.schedule.dao;

import com.coffeebreak.schedule.entity.Teacher;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: gera
 * Date: 17.03.14
 * Time: 21:52
 * To change this template use File | Settings | File Templates.
 */

public interface TeacherDao {

    public void saveTeacher(Teacher teacher);

    public void deleteTeacher(Teacher teacher);

    public Teacher findTeacherById(Integer id);

    public List<Teacher> findAllTeachers();
}