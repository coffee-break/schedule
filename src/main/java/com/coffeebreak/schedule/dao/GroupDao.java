package com.coffeebreak.schedule.dao;

import com.coffeebreak.schedule.entity.Group;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: gera
 * Date: 14.04.14
 * Time: 20:37
 * To change this template use File | Settings | File Templates.
 */

public interface GroupDao {

    public void saveGroup(Group group);

    public void deleteGroup(Group group);

    public Group findGroupById(Integer id);

    public List<Group> findAllGroups();
}