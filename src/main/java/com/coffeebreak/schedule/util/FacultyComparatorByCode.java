package com.coffeebreak.schedule.util;

import com.coffeebreak.schedule.entity.Faculty;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

/**
 * @author Arkady
 * @version 1.0, 02.04.14
 */

public class FacultyComparatorByCode implements Comparator<Faculty> {

    @Override
    public int compare(Faculty o1, Faculty o2) {

        String code1 = o1.getFacultyCode();
        String code2 = o2.getFacultyCode();

        Collator collator = Collator.getInstance(new Locale("Ru", "ru"));

        return collator.compare(code1, code2);
    }
}