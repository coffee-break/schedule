package com.coffeebreak.schedule.util;

import com.coffeebreak.schedule.entity.Department;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

/**
 * @author Arkady
 * @version 1.0, 03.04.14
 */

public class DepartmentComparatorByCode implements Comparator<Department> {

    @Override
    public int compare(Department o1, Department o2) {

        String code1 = o1.getDepartmentCode();
        String code2 = o2.getDepartmentCode();

        Collator collator = Collator.getInstance(Locale.ENGLISH);

        return collator.compare(code1, code2);
    }
}
