package com.coffeebreak.schedule.enums;

/**
 * @author Arkady
 * @version 1.0, 03.05.14
 */

public enum PairDay {

    MONDAY(1, "monday"),
    TUESDAY(2, "tuesday"),
    WEDNESDAY(3, "wednesday"),
    THURSDAY(4, "thursday"),
    FRIDAY(5, "friday"),
    SATURDAY(6, "saturday"),
    SUNDAY(7, "sunday");

    private int code;
    private String description;

    private PairDay(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public static PairDay getDayByCode(int code) {
        PairDay pairDay = null;
        for (PairDay day : PairDay.values()) {
            if (code == day.getCode()) {
                pairDay = day;
            }
        }
        return pairDay;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}