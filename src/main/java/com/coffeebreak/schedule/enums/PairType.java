package com.coffeebreak.schedule.enums;

/**
 * @author Arkady
 * @version 1.0, 03.05.14
 */

public enum PairType {

    LECTURE(1, "lecture"),
    SEMINAR(2, "seminar"),
    LAB(3, "lab");

    private int code;
    private String description;

    PairType(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}