package com.coffeebreak.schedule.controller;

import com.coffeebreak.schedule.entity.Department;
import com.coffeebreak.schedule.entity.Faculty;
import com.coffeebreak.schedule.service.DepartmentService;
import com.coffeebreak.schedule.service.FacultyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * @author Arkady
 * @version 1.0, 16.03.14
 */

@Controller
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private FacultyService facultyService;

    @RequestMapping("/departments.act")
    public String listDepartments(ModelMap model) {

        model.addAttribute("departments", departmentService.findAllDepartments());
        model.addAttribute("faculties", facultyService.findAllFaculties());

        return "departments";
    }

    @RequestMapping(value = "/department/add.act", method = RequestMethod.POST)
    public String addDepartment(@ModelAttribute("department") Department department,
                                @RequestParam("facultyId") int facultyId) {

        Faculty faculty = facultyService.findFacultyById(facultyId);

        if (faculty != null && department != null) {
            department.setFaculty(faculty);
            faculty.getDepartments().add(department);
            departmentService.addDepartment(department);
        }

        return "redirect:/departments.act";
    }

    @RequestMapping(value = "/department.act", method = RequestMethod.GET)
    public String department(@RequestParam("departmentId") int departmentId, ModelMap model) {

        if (departmentId != 0) {
            Department department = departmentService.findDepartmentById(departmentId);
            model.addAttribute("department", department);
            model.addAttribute("faculties", facultyService.findAllFaculties());
        }

        return "department";
    }

    @RequestMapping(value = "/department/delete.act/{departmentId}", method = RequestMethod.GET)
    public String deleteDepartment(@PathVariable("departmentId") int departmentId) {

        if (departmentId != 0) {
            Department department = departmentService.findDepartmentById(departmentId);
            if (department != null) {
                departmentService.deleteDepartment(department);
            }
        }

        return "redirect:/departments.act";
    }

    @RequestMapping(value = "/department/update.act", method = RequestMethod.POST)
    public String updateDepartment(@ModelAttribute("department") Department department,
                                   @RequestParam("facultyId") int facultyId) {

        if (department != null) {
            Faculty faculty = facultyService.findFacultyById(facultyId);
            department.setFaculty(faculty);
            faculty.getDepartments().add(department);
            departmentService.updateDepartment(department);
        }

        return "redirect:/department.act?departmentId=" + department.getDepartmentId();
    }
}