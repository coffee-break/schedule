package com.coffeebreak.schedule.controller;

import com.coffeebreak.schedule.entity.Department;
import com.coffeebreak.schedule.entity.Subject;
import com.coffeebreak.schedule.entity.Teacher;
import com.coffeebreak.schedule.service.DepartmentService;
import com.coffeebreak.schedule.service.FacultyService;
import com.coffeebreak.schedule.service.SubjectService;
import com.coffeebreak.schedule.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: gera
 * Date: 17.03.14
 * Time: 21:49
 * To change this template use File | Settings | File Templates.
 */

@Controller
public class TeacherController {

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private FacultyService facultyService;

    @Autowired
    private SubjectService subjectService;

    @RequestMapping("/teachers.act")
    public String listTeachers(ModelMap model) {

        model.addAttribute("teachers", teacherService.findAllTeachers());
        model.addAttribute("faculties", facultyService.findAllFaculties());

        return "teachers";
    }

    @RequestMapping(value = "/teacher.act", method = RequestMethod.GET)
    public String teacher(@RequestParam("teacherId") int teacherId, ModelMap model) {

        if (teacherId != 0) {
            Teacher teacher = teacherService.findTeacherById(teacherId);
            model.addAttribute("teacher", teacher);
            model.addAttribute("teacherSubjects", teacher.getSubjects());
            model.addAttribute("faculties", facultyService.findAllFaculties());
            model.addAttribute("facultyDepartments", teacher.getDepartment().getFaculty().getDepartments());
        }

        return "teacher";
    }

    @RequestMapping(value = "/teacher/add.act", method = RequestMethod.POST)
    public String addTeacher(@ModelAttribute("teacher") Teacher teacher,
                             @RequestParam("departmentId") int departmentId) {

        Department department = departmentService.findDepartmentById(departmentId);

        if (department != null) {
            teacher.setDepartment(department);
            department.getTeachers().add(teacher);
            teacherService.addTeacher(teacher);
        }

        return "redirect:/teachers.act";
    }

    @RequestMapping(value = "/teacher/delete.act/{teacherId}", method = RequestMethod.GET)
    public String deleteTeacher(@PathVariable("teacherId") int teacherId) {

        if (teacherId != 0) {
            Teacher teacher = teacherService.findTeacherById(teacherId);
            if (teacher != null) {
                teacherService.deleteTeacher(teacher);
            }
        }

        return "redirect:/teachers.act";
    }

    @RequestMapping(value = "/teacher/update.act", method = RequestMethod.POST)
    public String updateTeacher(@ModelAttribute("teacher") Teacher teacher,
                                @RequestParam("departmentId") int departmentId,
                                @RequestParam("subjectId") List<Integer> subjects) {

        if (teacher != null && departmentId != 0) {
            Department department = departmentService.findDepartmentById(departmentId);
            teacher.setDepartment(department);
            department.getTeachers().add(teacher);
            for (Integer subjectId : subjects) {
                Subject subject = subjectService.findSubjectById(subjectId);
                teacher.getSubjects().add(subject);
            }
            teacherService.updateTeacher(teacher);
        }

        return "redirect:/teacher.act?teacherId=" + teacher.getTeacherId();
    }

    @RequestMapping(value = "/deleteTeacherSubject.act", method = RequestMethod.POST)
    @ResponseBody
    public String deleteTeacherSubject(@RequestParam("subjectId") int subjectId,
                                       @RequestParam("teacherId") int teacherId) {

        if (subjectId != 0 && teacherId != 0) {
            Teacher teacher = teacherService.findTeacherById(teacherId);
            Subject subject = subjectService.findSubjectById(subjectId);
            if (teacher != null && subject != null) {
                if (teacher.getSubjects().contains(subject)) {
                    teacher.getSubjects().remove(subject);
                    teacherService.updateTeacher(teacher);
                }
            }
        }

        return "ok";
    }
}