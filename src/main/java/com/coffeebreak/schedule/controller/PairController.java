package com.coffeebreak.schedule.controller;

import com.coffeebreak.schedule.entity.Group;
import com.coffeebreak.schedule.entity.Pair;
import com.coffeebreak.schedule.entity.Subject;
import com.coffeebreak.schedule.entity.Teacher;
import com.coffeebreak.schedule.enums.PairDay;
import com.coffeebreak.schedule.enums.PairType;
import com.coffeebreak.schedule.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: gera
 * Date: 14.04.14
 * Time: 21:33
 * To change this template use File | Settings | File Templates.
 */

@Controller
public class PairController {

    @Autowired
    private PairService pairService;

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private FacultyService facultyService;

    @Autowired
    private TeacherService teacherService;

    @RequestMapping("/pairs.act")
    public String listPairs(ModelMap model) {

        model.addAttribute("pairs", pairService.findAllPairs());
        model.addAttribute("subjects", subjectService.findAllSubject());
        model.addAttribute("groups", groupService.findAllGroups());
        model.addAttribute("days", PairDay.values());
        model.addAttribute("types", PairType.values());
        model.addAttribute("faculties", facultyService.findAllFaculties());

        return "pairs";
    }

    @RequestMapping(value = "/pair.act", method = RequestMethod.GET)
    public String pair(@RequestParam("pairId") int pairId, ModelMap model) {

        if (pairId != 0) {
            Pair pair = pairService.findPairById(pairId);
            model.addAttribute("pair", pair);
            model.addAttribute("pairGroups", pair.getGroups());
            model.addAttribute("subjects", subjectService.findAllSubject());
            model.addAttribute("subjectTeachers", pair.getSubject().getTeachers());
        }

        model.addAttribute("days", PairDay.values());
        model.addAttribute("types", PairType.values());

        return "pair";
    }

    @RequestMapping(value = "/pair/add.act", method = RequestMethod.POST)
    public String addPair(@ModelAttribute("pair") Pair pair,
                          @RequestParam("subjectId") int subjectId,
                          @RequestParam("teacherId") int teacherId,
                          @RequestParam("groupId") List<Integer> groups) {

        if (pair != null && groups != null && !groups.isEmpty()) {

            for (Integer groupId : groups) {
                Group group = groupService.findGroupById(groupId);
                group.getPairs().add(pair);
                pair.getGroups().add(group);
            }

            Subject subject = subjectService.findSubjectById(subjectId);
            Teacher teacher = teacherService.findTeacherById(teacherId);

            pair.setSubject(subject);
            subject.getPairs().add(pair);

            pair.setTeacher(teacher);
            teacher.getPairs().add(pair);

            pairService.addPair(pair);
        }

        return "redirect:/pairs.act";
    }

    @RequestMapping(value = "/pair/delete.act/{pairId}", method = RequestMethod.GET)
    public String deletePair(@PathVariable("pairId") int pairId) {

        if (pairId != 0) {
            Pair pair = pairService.findPairById(pairId);
            if (pair != null) {
                for (Group group : pair.getGroups()) {
                    group.getPairs().remove(pair);
                    groupService.updateGroup(group);
                }
                pairService.deletePair(pair);
            }
        }

        return "redirect:/pairs.act";
    }

    @RequestMapping(value = "/pair/update.act", method = RequestMethod.POST)
    public String updatePair(@ModelAttribute("pair") Pair pair,
                             @RequestParam("subjectId") int subjectId,
                             @RequestParam("teacherId") int teacherId,
                             @RequestParam("groupId") List<Integer> groups) {

        if (pair != null) {

            for (Integer groupId : groups) {
                Group group = groupService.findGroupById(groupId);
                pair.getGroups().add(group);
            }


            Subject subject = subjectService.findSubjectById(subjectId);
            Teacher teacher = teacherService.findTeacherById(teacherId);

            pair.setSubject(subject);
            subject.getPairs().add(pair);

            pair.setTeacher(teacher);
            teacher.getPairs().add(pair);

            pairService.updatePair(pair);
        }

        return "redirect:/pair.act?pairId=" + pair.getPairId();
    }

    @RequestMapping(value = "/deletePairGroup.act", method = RequestMethod.POST)
    @ResponseBody
    public String deletePairGroup(@RequestParam("groupId") int groupId,
                                  @RequestParam("pairId") int pairId) {

        if (groupId != 0 && pairId != 0) {
            Pair pair = pairService.findPairById(pairId);
            Group group = groupService.findGroupById(groupId);
            group.getPairs().remove(pair);
            groupService.updateGroup(group);
        }

        return "ok";
    }
}