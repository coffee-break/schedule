package com.coffeebreak.schedule.controller;

import com.coffeebreak.schedule.entity.Pair;
import com.coffeebreak.schedule.enums.PairDay;
import com.coffeebreak.schedule.enums.PairType;
import com.coffeebreak.schedule.service.FacultyService;
import com.coffeebreak.schedule.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * @author Arkady Asabin
 * @version 1.0, 05.05.2014
 */

@Controller
public class ScheduleController {

    private final static int DAILY_SCHEDULE = 1;
    private final static int WEEKLY_SCHEDULE = 2;

    @Autowired
    private FacultyService facultyService;

    @Autowired
    private GroupService groupService;

    @RequestMapping("/dashboard.act")
    public String showDashboard(ModelMap model) {

        model.addAttribute("faculties", facultyService.findAllFaculties());
        model.addAttribute("days", PairDay.values());

        Calendar currentDate = Calendar.getInstance();
        model.addAttribute("currentDay", currentDate.get(Calendar.DAY_OF_WEEK) - 1);

        // Пусть пока текущий семестр начался с 3 февраля
        Calendar beginSemester = Calendar.getInstance();
        beginSemester.set(currentDate.get(Calendar.YEAR), Calendar.FEBRUARY, 3);
        int currentWeek = currentDate.get(Calendar.WEEK_OF_YEAR) - beginSemester.get(Calendar.WEEK_OF_YEAR) - 1;

        model.addAttribute("currentWeek", currentWeek % 2 != 0 ? 1 : 2);

        return "dashboard";
    }

    @RequestMapping(value = "/schedule.act", method = RequestMethod.POST)
    public String showSchedule(@RequestParam("groupId") int groupId,
                               @RequestParam("scheduleType") int scheduleType,
                               @RequestParam("scheduleWeek") int scheduleWeek,
                               @RequestParam("scheduleDay") int scheduleDay,
                               ModelMap model) {

        model.addAttribute("scheduleType", scheduleType);

        if (scheduleType == DAILY_SCHEDULE) {
            model.addAttribute("scheduleDay", PairDay.getDayByCode(scheduleDay).getDescription());
            model.addAttribute("scheduleWeek", scheduleWeek);
            model.addAttribute("pairs", getDailySchedule(groupId, scheduleDay, scheduleWeek));
            model.addAttribute("pairTypes", PairType.values());
        }

        return "schedule/schedule";
    }

    public List<Pair> getDailySchedule(int groupId, int scheduleDay, int scheduleWeek) {
        List<Pair> pairs = new ArrayList<Pair>();

        for (Pair pair : groupService.findGroupById(groupId).getPairs()) {
            if (pair.getDay() == scheduleDay && pair.getWeek() == scheduleWeek) {
                pairs.add(pair);
            }
        }

        return pairs;
    }
}