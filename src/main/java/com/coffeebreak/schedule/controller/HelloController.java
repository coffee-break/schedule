package com.coffeebreak.schedule.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class HelloController {
	@RequestMapping()
	public String printWelcome() {
		return "redirect:/dashboard.act";
	}
}