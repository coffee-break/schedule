package com.coffeebreak.schedule.controller;

import com.coffeebreak.schedule.service.DepartmentService;
import com.coffeebreak.schedule.service.FacultyService;
import com.coffeebreak.schedule.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Arkady
 * @version 1.0, 16.04.14
 */

@Controller
public class GeneralController {

    @Autowired
    private FacultyService facultyService;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private SubjectService subjectService;

    @RequestMapping(value = "/showDropDownDepartments.act", method = RequestMethod.POST)
    public String showDropDownDepartments(@RequestParam("facultyId") int facultyId, ModelMap model) {

        model.addAttribute("departments", facultyService.findFacultyById(facultyId).getDepartments());

        return "dynamic/dropDownDepartments";
    }

    @RequestMapping(value = "/showCheckBoxTeachers.act", method = RequestMethod.POST)
    public String showCheckBoxTeachers(@RequestParam("departmentId") int departmentId, ModelMap model) {

        model.addAttribute("teachers", departmentService.findDepartmentById(departmentId).getTeachers());

        return "dynamic/checkBoxTeachers";
    }

    @RequestMapping(value = "/showCheckBoxGroups.act", method = RequestMethod.POST)
    public String showCheckBoxGroups(@RequestParam("departmentId") int departmentId, ModelMap model) {

        model.addAttribute("groups", departmentService.findDepartmentById(departmentId).getGroups());

        return "dynamic/checkBoxGroups";
    }

    @RequestMapping(value = "/showDropDownGroups.act", method = RequestMethod.POST)
    public String showDropDownGroups(@RequestParam("departmentId") int departmentId, ModelMap model) {

        model.addAttribute("groups", departmentService.findDepartmentById(departmentId).getGroups());

        return "dynamic/dropDownGroups";
    }

    @RequestMapping(value = "/showDropDownTeachers.act", method = RequestMethod.POST)
    public String showDropDownTeachers(@RequestParam("subjectId") int subjectId, ModelMap model) {

        model.addAttribute("teachers", subjectService.findSubjectById(subjectId).getTeachers());

        return "dynamic/dropDownTeachers";
    }
}