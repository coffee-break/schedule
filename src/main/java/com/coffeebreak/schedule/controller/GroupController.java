package com.coffeebreak.schedule.controller;

import com.coffeebreak.schedule.entity.Department;
import com.coffeebreak.schedule.entity.Group;
import com.coffeebreak.schedule.entity.Pair;
import com.coffeebreak.schedule.enums.PairDay;
import com.coffeebreak.schedule.enums.PairType;
import com.coffeebreak.schedule.service.DepartmentService;
import com.coffeebreak.schedule.service.FacultyService;
import com.coffeebreak.schedule.service.GroupService;
import com.coffeebreak.schedule.service.PairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: gera
 * Date: 14.04.14
 * Time: 22:32
 * To change this template use File | Settings | File Templates.
 */

@Controller
public class GroupController {

    @Autowired
    private GroupService groupService;

    @Autowired
    private PairService pairService;

    @Autowired
    private FacultyService facultyService;

    @Autowired
    private DepartmentService departmentService;

    @RequestMapping("/groups.act")
    public String listGroups(ModelMap model) {

        model.addAttribute("groups", groupService.findAllGroups());
        model.addAttribute("faculties", facultyService.findAllFaculties());

        return "groups";
    }

    @RequestMapping(value = "/group.act", method = RequestMethod.GET)
    public String group(@RequestParam("groupId") int groupId, ModelMap model) {

        if (groupId != 0) {
            Group group = groupService.findGroupById(groupId);
            model.addAttribute("group", group);
            model.addAttribute("groupPairs", group.getPairs());
            model.addAttribute("faculties", facultyService.findAllFaculties());
            model.addAttribute("facultyDepartments", group.getDepartment().getFaculty().getDepartments());
        }

        model.addAttribute("days", PairDay.values());
        model.addAttribute("types", PairType.values());

        return "group";
    }

    @RequestMapping(value = "/group/add.act", method = RequestMethod.POST)
    public String addGroup(@ModelAttribute("group") Group group,
                           @RequestParam("departmentId") int departmentId) {

        Department department = departmentService.findDepartmentById(departmentId);

        if (group != null) {
            group.setDepartment(department);
            department.getGroups().add(group);
            groupService.addGroup(group);
        }

        return "redirect:/groups.act";
    }

    @RequestMapping(value = "/group/delete.act/{groupId}", method = RequestMethod.GET)
    public String deleteGroup(@PathVariable("groupId") int groupId) {

        if (groupId != 0) {
            Group group = groupService.findGroupById(groupId);
            if (group != null) {
                groupService.deleteGroup(group);
            }
        }

        return "redirect:/groups.act";
    }

    @RequestMapping(value = "/group/update.act", method = RequestMethod.POST)
    public String updateGroup(@ModelAttribute("group") Group group,
                              @RequestParam("departmentId") int departmentId,
                              @RequestParam("pairId") List<Integer> pairs) {

        if (group != null) {
            Department department = departmentService.findDepartmentById(departmentId);
            group.setDepartment(department);
            department.getGroups().add(group);
            for (int pairId : pairs) {
                Pair pair = pairService.findPairById(pairId);
                group.getPairs().add(pair);
            }
            groupService.updateGroup(group);
        }

        return "redirect:/group.act?groupId=" + group.getGroupId();
    }

    @RequestMapping(value = "/deleteGroupPair.act", method = RequestMethod.POST)
    @ResponseBody
    public String deleteGroupPair(@RequestParam("pairId") int pairId,
                                  @RequestParam("groupId") int groupId) {

        if (pairId != 0 && groupId != 0) {
            Group group = groupService.findGroupById(groupId);
            Pair pair = pairService.findPairById(pairId);
            if (group != null && pair != null) {
                if (group.getPairs().contains(pair)) {
                    group.getPairs().remove(pair);
                    groupService.updateGroup(group);
                }
            }
        }

        return "ok";
    }
}