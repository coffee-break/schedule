package com.coffeebreak.schedule.controller;

import com.coffeebreak.schedule.entity.Department;
import com.coffeebreak.schedule.entity.Faculty;
import com.coffeebreak.schedule.service.DepartmentService;
import com.coffeebreak.schedule.service.FacultyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * @author Arkady
 * @version 1.0, 13.03.14
 */

@Controller
public class FacultyController {

    @Autowired
    private FacultyService facultyService;

    @Autowired
    private DepartmentService departmentService;

    @RequestMapping("/faculties.act")
    public String listFaculties(ModelMap model) {

        model.addAttribute("faculties", facultyService.findAllFaculties());

        return "faculties";
    }

    @RequestMapping(value = "/faculty/add.act", method = RequestMethod.POST)
    public String addFaculty(@ModelAttribute("faculty") Faculty faculty) {

        if (faculty != null) {
            facultyService.addFaculty(faculty);
        }

        return "redirect:/faculties.act";
    }

    @RequestMapping(value = "/faculty.act", method = RequestMethod.GET)
    public String faculty(@RequestParam("facultyId") int facultyId, ModelMap model) {

        if (facultyId != 0) {
            Faculty faculty = facultyService.findFacultyById(facultyId);
            model.addAttribute("faculty", faculty);
            model.addAttribute("facultyDepartments", faculty.getDepartments());
        }

        return "faculty";
    }

    @RequestMapping(value = "/faculty/delete.act/{facultyId}", method = RequestMethod.GET)
    public String deleteFaculty(@PathVariable("facultyId") int facultyId) {

        if (facultyId != 0) {
            Faculty faculty = facultyService.findFacultyById(facultyId);
            if (faculty != null) {
                facultyService.deleteFaculty(faculty);
            }
        }

        return "redirect:/faculties.act";
    }

    @RequestMapping(value = "/faculty/update.act", method = RequestMethod.POST)
    public String updateFaculty(@ModelAttribute("faculty") Faculty faculty) {

        if (faculty != null) {
            facultyService.updateFaculty(faculty);
        }

        return "redirect:/faculty.act?facultyId=" + faculty.getFacultyId();
    }

    @RequestMapping(value = "/deleteFacultyDepartment.act", method = RequestMethod.POST)
    @ResponseBody
    public String deleteFacultyDepartment(@RequestParam("departmentId") int departmentId) {

        if (departmentId != 0) {
            Department department = departmentService.findDepartmentById(departmentId);
            if (department != null) {
                departmentService.deleteDepartment(department);
            }
        } else {
            return "not ok";
        }

        return "ok";
    }
}