package com.coffeebreak.schedule.controller;

import com.coffeebreak.schedule.entity.Subject;
import com.coffeebreak.schedule.entity.Teacher;
import com.coffeebreak.schedule.service.FacultyService;
import com.coffeebreak.schedule.service.SubjectService;
import com.coffeebreak.schedule.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: gera
 * Date: 03.04.14
 * Time: 20:49
 * To change this template use File | Settings | File Templates.
 */

@Controller
public class SubjectController {

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private FacultyService facultyService;

    @RequestMapping("/subjects.act")
    public String listSubjects(ModelMap model) {

        model.addAttribute("subjects", subjectService.findAllSubject());
        model.addAttribute("faculties", facultyService.findAllFaculties());

        return "subjects";
    }

    @RequestMapping(value = "/subject.act", method = RequestMethod.GET)
    public String subject(@RequestParam("subjectId") int subjectId, ModelMap model) {

        if (subjectId != 0) {
            Subject subject = subjectService.findSubjectById(subjectId);
            model.addAttribute("subject", subject);
            model.addAttribute("subjectTeachers", subject.getTeachers());
        }

        return "subject";
    }

    @RequestMapping(value = "/subject/add.act", method = RequestMethod.POST)
    public String addSubject(@ModelAttribute("subject") Subject subject,
                             @RequestParam("teacherId") List<Integer> teachers) {

        if (subject != null && teachers != null && !teachers.isEmpty()) {

            for (Integer teacherId : teachers) {
                Teacher teacher = teacherService.findTeacherById(teacherId);
                teacher.getSubjects().add(subject);
                subject.getTeachers().add(teacher);
            }

            subjectService.addSubject(subject);
        }

        return "redirect:/subjects.act";
    }

    @RequestMapping(value = "/subject/delete.act/{subjectId}", method = RequestMethod.GET)
    public String deleteSubject(@PathVariable("subjectId") int subjectId) {

        if (subjectId != 0) {
            Subject subject = subjectService.findSubjectById(subjectId);
            if (subject != null) {
                for (Teacher teacher : subject.getTeachers()) {
                    teacher.getSubjects().remove(subject);
                    teacherService.updateTeacher(teacher);
                }
                subjectService.deleteSubject(subject);
            }
        }

        return "redirect:/subjects.act";
    }

    @RequestMapping(value = "/subject/update.act", method = RequestMethod.POST)
    public String updateSubject(@ModelAttribute("subject") Subject subject,
                                @RequestParam("teacherId") List<Integer> teachers) {

        if (subject != null) {
            for (Integer teacherId : teachers) {
                Teacher teacher = teacherService.findTeacherById(teacherId);
                subject.getTeachers().add(teacher);
            }
            subjectService.updateSubject(subject);
        }

        return "redirect:/subject.act?subjectId=" + subject.getSubjectId();
    }

    @RequestMapping(value = "/deleteSubjectTeacher.act", method = RequestMethod.POST)
    @ResponseBody
    public String deleteSubjectTeacher(@RequestParam("teacherId") int teacherId,
                                       @RequestParam("subjectId") int subjectId) {

        if (subjectId != 0 && teacherId != 0) {
            Teacher teacher = teacherService.findTeacherById(teacherId);
            Subject subject = subjectService.findSubjectById(subjectId);
            teacher.getSubjects().remove(subject);
            teacherService.updateTeacher(teacher);
        }

        return "ok";
    }
}