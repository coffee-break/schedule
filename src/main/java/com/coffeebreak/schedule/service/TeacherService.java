package com.coffeebreak.schedule.service;

import com.coffeebreak.schedule.entity.Teacher;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: gera
 * Date: 17.03.14
 * Time: 22:17
 * To change this template use File | Settings | File Templates.
 */

public interface TeacherService {

    public void addTeacher(Teacher teacher);

    public void updateTeacher(Teacher teacher);

    public void deleteTeacher(Teacher teacher);

    public Teacher findTeacherById(Integer id);

    public List<Teacher> findAllTeachers();
}