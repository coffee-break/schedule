package com.coffeebreak.schedule.service;

import com.coffeebreak.schedule.entity.Faculty;

import java.util.List;

/**
 * @author Arkady
 * @version 1.0, 13.03.14
 */

public interface FacultyService {

    public void addFaculty(Faculty faculty);

    public void updateFaculty(Faculty faculty);

    public void deleteFaculty(Faculty faculty);

    public Faculty findFacultyById(Integer id);

    public List<Faculty> findAllFaculties();
}