package com.coffeebreak.schedule.service.impl;

import com.coffeebreak.schedule.dao.DepartmentDao;
import com.coffeebreak.schedule.entity.Department;
import com.coffeebreak.schedule.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Arkady
 * @version 1.0, 16.03.14
 */

@Transactional
@Service("departmentService")
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    private DepartmentDao departmentDao;

    @Override
    public void addDepartment(Department department) {
        departmentDao.saveDepartment(department);
    }

    @Override
    public void updateDepartment(Department department) {
        departmentDao.saveDepartment(department);
    }

    @Override
    public void deleteDepartment(Department department) {
        departmentDao.deleteDepartment(department);
    }

    @Override
    @Transactional(readOnly = true)
    public Department findDepartmentById(Integer id) {
        return departmentDao.findDepartmentById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Department> findAllDepartments() {
        return departmentDao.findAllDepartments();
    }
}