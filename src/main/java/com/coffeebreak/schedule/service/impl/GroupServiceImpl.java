package com.coffeebreak.schedule.service.impl;

import com.coffeebreak.schedule.entity.Group;
import com.coffeebreak.schedule.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.coffeebreak.schedule.dao.GroupDao;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: gera
 * Date: 14.04.14
 * Time: 21:02
 * To change this template use File | Settings | File Templates.
 */

@Transactional
@Service("groupService")
public class GroupServiceImpl implements GroupService {

    @Autowired
    private GroupDao groupDao;

    @Override
    public void addGroup(Group group) {
        groupDao.saveGroup(group);
    }

    @Override
    public void updateGroup(Group group) {
        groupDao.saveGroup(group);
    }

    @Override
    public void deleteGroup(Group group) {
        groupDao.deleteGroup(group);
    }

    @Override
    @Transactional(readOnly = true)
    public Group findGroupById(Integer id) {
        return groupDao.findGroupById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Group> findAllGroups() {
        return groupDao.findAllGroups();
    }
}