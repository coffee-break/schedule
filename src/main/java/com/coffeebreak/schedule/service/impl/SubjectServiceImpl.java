package com.coffeebreak.schedule.service.impl;

import com.coffeebreak.schedule.dao.SubjectDao;
import com.coffeebreak.schedule.entity.Subject;
import com.coffeebreak.schedule.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: gera
 * Date: 03.04.14
 * Time: 20:39
 * To change this template use File | Settings | File Templates.
 */

@Transactional
@Service("subjectService")
public class SubjectServiceImpl implements SubjectService {

    @Autowired
    private SubjectDao subjectDao;

    @Override
    public void addSubject(Subject subject) {
        subjectDao.saveSubject(subject);
    }

    @Override
    public void updateSubject(Subject subject) {
        subjectDao.saveSubject(subject);
    }

    @Override
    public void deleteSubject(Subject subject) {
        subjectDao.deleteSubject(subject);
    }

    @Override
    @Transactional(readOnly = true)
    public Subject findSubjectById(Integer id) {
        return subjectDao.findSubjectById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Subject> findAllSubject() {
        return subjectDao.findAllSubject();
    }
}