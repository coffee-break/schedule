package com.coffeebreak.schedule.service.impl;

import com.coffeebreak.schedule.entity.Teacher;
import com.coffeebreak.schedule.service.TeacherService;
import com.coffeebreak.schedule.dao.TeacherDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: gera
 * Date: 17.03.14
 * Time: 22:21
 * To change this template use File | Settings | File Templates.
 */

@Transactional
@Service("teacherService")
public class TeacherServiceImpl implements TeacherService {

    @Autowired
    private TeacherDao teacherDao;

    @Override
    public void addTeacher(Teacher teacher) {
        teacherDao.saveTeacher(teacher);
    }

    @Override
    public void updateTeacher(Teacher teacher) {
        teacherDao.saveTeacher(teacher);
    }

    @Override
    public void deleteTeacher(Teacher teacher) {
        teacherDao.deleteTeacher(teacher);
    }

    @Override
    @Transactional(readOnly = true)
    public Teacher findTeacherById(Integer id) {
        return teacherDao.findTeacherById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Teacher> findAllTeachers() {
        return teacherDao.findAllTeachers();
    }
}