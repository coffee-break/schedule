package com.coffeebreak.schedule.service.impl;

import com.coffeebreak.schedule.dao.FacultyDao;
import com.coffeebreak.schedule.entity.Faculty;
import com.coffeebreak.schedule.service.FacultyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Arkady
 * @version 1.0, 13.03.14
 */

@Transactional
@Service("facultyService")
public class FacultyServiceImpl implements FacultyService {

    @Autowired
    private FacultyDao facultyDao;

    @Override
    public void addFaculty(Faculty faculty) {
        facultyDao.saveFaculty(faculty);
    }

    @Override
    public void updateFaculty(Faculty faculty) {
        facultyDao.saveFaculty(faculty);
    }

    @Override
    public void deleteFaculty(Faculty faculty) {
        facultyDao.deleteFaculty(faculty);
    }

    @Override
    @Transactional(readOnly = true)
    public Faculty findFacultyById(Integer id) {
        return facultyDao.findFacultyById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Faculty> findAllFaculties() {
        return facultyDao.findAllFaculties();
    }
}