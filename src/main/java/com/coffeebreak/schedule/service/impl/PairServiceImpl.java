package com.coffeebreak.schedule.service.impl;

import com.coffeebreak.schedule.dao.PairDao;
import com.coffeebreak.schedule.entity.Pair;
import com.coffeebreak.schedule.service.PairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: gera
 * Date: 14.04.14
 * Time: 20:11
 * To change this template use File | Settings | File Templates.
 */

@Transactional
@Service("pairService")
public class PairServiceImpl implements PairService {

    @Autowired
    private PairDao pairDao;

    @Override
    public void addPair(Pair pair) {
        pairDao.savePair(pair);
    }

    @Override
    public void updatePair(Pair pair) {
        pairDao.savePair(pair);
    }

    @Override
    public void deletePair(Pair pair) {
        pairDao.deletePair(pair);
    }

    @Override
    @Transactional(readOnly = true)
    public Pair findPairById(Integer id) {
        return pairDao.findPairById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Pair> findAllPairs() {
        return pairDao.findAllPairs();
    }
}