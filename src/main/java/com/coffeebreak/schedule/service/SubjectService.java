package com.coffeebreak.schedule.service;

import com.coffeebreak.schedule.entity.Subject;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: gera
 * Date: 03.04.14
 * Time: 20:38
 * To change this template use File | Settings | File Templates.
 */

public interface SubjectService {

    public void addSubject(Subject subject);

    public void updateSubject(Subject subject);

    public void deleteSubject(Subject subject);

    public Subject findSubjectById(Integer id);

    public List<Subject> findAllSubject();
}