package com.coffeebreak.schedule.service;

import com.coffeebreak.schedule.entity.Pair;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: gera
 * Date: 14.04.14
 * Time: 20:09
 * To change this template use File | Settings | File Templates.
 */

public interface PairService {

    public void addPair(Pair pair);

    public void updatePair(Pair pair);

    public void deletePair(Pair pair);

    public Pair findPairById(Integer id);

    public List<Pair> findAllPairs();
}