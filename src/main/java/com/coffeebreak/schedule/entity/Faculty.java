package com.coffeebreak.schedule.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * @author Arkady
 * @version 1.0, 13.03.14
 */

@Entity
@Table(name = "faculty")
public class Faculty {

    private Integer facultyId;
    private String facultyCode;
    private String facultyName;
    private List<Department> departments = new ArrayList<Department>();

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "faculty_id", unique = true, nullable = false)
    public Integer getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(Integer facultyId) {
        this.facultyId = facultyId;
    }

    @Column(name = "faculty_code", unique = true, nullable = false, length = 10)
    public String getFacultyCode() {
        return facultyCode;
    }

    public void setFacultyCode(String facultyCode) {
        this.facultyCode = facultyCode;
    }

    @Column(name = "faculty_name", unique = true, nullable = false, length = 45)
    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    @OneToMany(mappedBy = "faculty", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }
}