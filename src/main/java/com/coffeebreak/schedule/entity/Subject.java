package com.coffeebreak.schedule.entity;

import javax.persistence.*;
import java.util.List;
import java.util.ArrayList;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created with IntelliJ IDEA.
 * User: gera
 * Date: 03.04.14
 * Time: 19:49
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name = "subject")
public class Subject {

    private Integer subjectId;
    private String subjectCode;
    private String subjectName;
    private Integer hours;
    private Integer controlPoints;
    private Integer finalControl;
    private List<Teacher> teachers = new ArrayList<Teacher>();
    private List<Pair> pairs = new ArrayList<Pair>();

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "subject_id", unique = true, nullable = false)
    public Integer getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }

    @Column(name = "subject_code", nullable = false, length = 10)
    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    @Column(name = "subject_name", nullable = false, length = 45)
    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    @Column(name = "hours", nullable = false)
    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    @Column(name = "control_points", nullable = false)
    public Integer getControlPoints() {
        return controlPoints;
    }

    public void setControlPoints(Integer controlPoints) {
        this.controlPoints = controlPoints;
    }

    @Column(name = "final_control", nullable = false)
    public Integer getFinalControl() {
        return finalControl;
    }

    public void setFinalControl(Integer finalControl) {
        this.finalControl = finalControl;
    }

    @ManyToMany(mappedBy = "subjects")
    public List<Teacher> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<Teacher> teachers) {
        this.teachers = teachers;
    }

    @OneToMany(mappedBy = "subject", cascade = CascadeType.ALL, orphanRemoval = true)
    public List<Pair> getPairs() {
        return pairs;
    }

    public void setPairs(List<Pair> pairs) {
        this.pairs = pairs;
    }
}