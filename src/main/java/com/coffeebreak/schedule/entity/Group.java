package com.coffeebreak.schedule.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created with IntelliJ IDEA.
 * User: gera
 * Date: 03.04.14
 * Time: 19:41
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name = "`group`")
public class Group {

    private Integer groupId;
    private String groupCode;
    private Integer course;
    private Department department;
    private List<Pair> pairs = new ArrayList<Pair>();

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "group_id", unique = true, nullable = false)
    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    @Column(name = "group_code", nullable = false, length = 10)
    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    @Column(name = "course", nullable = false)
    public Integer getCourse() {
        return course;
    }

    public void setCourse(Integer course) {
        this.course = course;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "department_id")
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @ManyToMany
    @JoinTable(name = "group_has_pair",
            joinColumns = @JoinColumn(name = "group_id", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "pair_id", nullable = false))
    public List<Pair> getPairs() {
        return pairs;
    }

    public void setPairs(List<Pair> pairs) {
        this.pairs = pairs;
    }
}