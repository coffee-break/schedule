<#-- @ftlvariable name="faculty" type="com.coffeebreak.schedule.entity.Faculty" -->
<#-- @ftlvariable name="facultyDepartments" type="java.util.List<com.coffeebreak.schedule.entity.Department>" -->

<#include "macros/general.ftl"/>

<@header "faculty"/>

<h3><@spring.message "faculty"/> ${faculty.facultyCode!""}</h3>

<form method="post" action="faculty/update.act">
    <table>
        <tr>
            <td><@spring.message "code"/>:</td>
            <td>
                <input type="text" name="facultyCode" value="${faculty.facultyCode!""}">
            </td>
        </tr>
        <tr>
            <td><@spring.message "name"/>:</td>
            <td>
                <input type="text" name="facultyName" value="${faculty.facultyName!""}">
            </td>
        </tr>
        <tr>
            <td>Departments:</td>
            <td>
            <#if facultyDepartments??>
                <#list facultyDepartments?sort_by("departmentCode") as department>
                    <div id="facultyDepartment${department_index + 1}">
                    ${department_index + 1}
                        <a href="department.act?departmentId=${department.departmentId!"0"}">${department.departmentCode!""}</a>
                        <input type="button" value="x"
                               onclick="deleteFacultyDepartment('facultyDepartment${department_index + 1}', '${department.departmentId!"0"}');">
                    </div>
                </#list>
            </#if>
            </td>
        </tr>
        <tr>
            <td><input type="submit" value="<@spring.message "update"/>"></td>
        </tr>
    </table>

    <input type="hidden" name="facultyId" value="${faculty.facultyId!"0"}">
</form>

<@footer "none"/>