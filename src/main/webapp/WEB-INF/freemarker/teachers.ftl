<#-- @ftlvariable name="faculties" type="java.util.List<com.coffeebreak.schedule.entity.Faculty>" -->
<#-- @ftlvariable name="teachers" type="java.util.List<com.coffeebreak.schedule.entity.Teacher>" -->

<#include "macros/general.ftl"/>

<@header "teachers"/>

<form method="post" action="teacher/add.act">
    <table>
        <tr>
            <td><@spring.message "shortName"/></td>
            <td>
                <input type="text" name="shortName">
            </td>
        </tr>
        <tr>
            <td><@spring.message "firstName"/></td>
            <td>
                <input type="text" name="firstName">
            </td>
        </tr>
        <tr>
            <td><@spring.message "lastName"/></td>
            <td>
                <input type="text" name="lastName">
            </td>
        </tr>
        <tr>
            <td><@spring.message "middleName"/></td>
            <td>
                <input type="text" name="middleName">
            </td>
        </tr>
        <tr>
            <td><@spring.message "faculty"/></td>
            <td>
                <select id="facultyId" onchange="showDropDownDepartments('teacher');">
                <#if faculties??>
                    <option value="0"><@spring.message "select"/></option>
                    <#list faculties?sort_by("facultyCode") as faculty>
                        <option value="${faculty.facultyId}">${faculty.facultyCode}</option>
                    </#list>
                </#if>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "department"/></td>
            <td>
                <select id="departmentId" name="departmentId">
                    <option value="0"><@spring.message "select"/></option>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" value="<@spring.message "add"/>"/>
            </td>
        </tr>
    </table>
</form>

<h3><@spring.message "listTeachers"/></h3>
<#if teachers??>
<table>
    <tr>
        <th>#</th>
        <th><@spring.message "shortName"/></th>
        <th><@spring.message "firstName"/></th>
        <th><@spring.message "lastName"/></th>
        <th><@spring.message "middleName"/></th>
        <th><@spring.message "department"/></th>
        <th>x</th>
    </tr>
    <#list teachers as teacher>
        <tr>
            <td>${teacher_index + 1}</td>
            <td>
                <a href="teacher.act?teacherId=${teacher.teacherId!"0"}">${teacher.shortName!""}</a>
            </td>
            <td>${teacher.firstName!""}</td>
            <td>${teacher.lastName!""}</td>
            <td>${teacher.middleName!""}</td>
            <td>
                <a href="department.act?departmentId=${teacher.department.departmentId!"0"}">${teacher.department.departmentCode!""}</a>
            </td>
            <td>
                <input type="button" value="x" onclick="deleteEntity('teacher', '${teacher.teacherId!"0"}');">
            </td>
        </tr>
    </#list>
</table>
</#if>

<@footer "teachers"/>