<#-- @ftlvariable name="faculties" type="java.util.List<com.coffeebreak.schedule.entity.Faculty>" -->
<#-- @ftlvariable name="subjects" type="java.util.List<com.coffeebreak.schedule.entity.Subject>" -->

<#include "macros/general.ftl"/>

<@header "subjects"/>

<form method="post" action="subject/add.act">
    <table>
        <tr>
            <td><@spring.message "code"/></td>
            <td>
                <input type="text" name="subjectCode">
            </td>
        </tr>
        <tr>
            <td><@spring.message "name"/></td>
            <td>
                <input type="text" name="subjectName">
            </td>
        </tr>
        <tr>
            <td><@spring.message "hours"/></td>
            <td>
                <input type="text" name="hours">
            </td>
        </tr>
        <tr>
            <td><@spring.message "controlPoints"/></td>
            <td>
                <input type="text" name="controlPoints">
            </td>
        </tr>
        <tr>
            <td><@spring.message "finalControl"/></td>
            <td>
                <select name="finalControl">
                    <option value="0"><@spring.message "select"/></option>
                    <option value="1"><@spring.message "noExam"/></option>
                    <option value="2"><@spring.message "exam"/></option>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "faculty"/></td>
            <td>
                <select id="facultyId" onchange="showDropDownDepartments('subject');">
                <#if faculties??>
                    <option value="0"><@spring.message "select"/></option>
                    <#list faculties?sort_by("facultyCode") as faculty>
                        <option value="${faculty.facultyId}">${faculty.facultyCode}</option>
                    </#list>
                </#if>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "department"/></td>
            <td>
                <select id="departmentId" name="departmentId">
                    <option value="0"><@spring.message "select"/></option>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "teacher"/></td>
            <td>
                <div id="checkBoxTeachers"></div>
            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" value="<@spring.message "add"/>"/>
            </td>
        </tr>
    </table>
</form>

<h3><@spring.message "listSubjects"/></h3>
<#if subjects??>
<table>
    <tr>
        <th>#</th>
        <th><@spring.message "code"/></th>
        <th><@spring.message "name"/></th>
        <th><@spring.message "hours"/></th>
        <th><@spring.message "controlPoints"/></th>
        <th><@spring.message "finalControl"/></th>
        <th>x</th>
    </tr>
    <#list subjects as subject>
        <tr>
            <td>${subject_index + 1}</td>
            <td>
                <a href="subject.act?subjectId=${subject.subjectId!"0"}">${subject.subjectCode!""}</a>
            </td>
            <td>${subject.subjectName!""}</td>
            <td>${subject.hours!""}</td>
            <td>${subject.controlPoints!""}</td>
            <td>
            <#if subject.finalControl?? && subject.finalControl == 1>
                <@spring.message "noExam"/>
            <#elseif subject.finalControl?? && subject.finalControl == 2>
                <@spring.message "exam"/>
            <#else>
                &nbsp;
            </#if>
            </td>
            <td>
                <input type="button" value="x" onclick="deleteEntity('subject', '${subject.subjectId!"0"}');">
            </td>
        </tr>
    </#list>
</table>
</#if>

<@footer "subjects"/>