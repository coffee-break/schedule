<#-- @ftlvariable name="subject" type="com.coffeebreak.schedule.entity.Subject" -->
<#-- @ftlvariable name="subjectTeachers" type="java.util.List<com.coffeebreak.schedule.entity.Teacher>" -->

<#include "macros/general.ftl"/>

<@header "subject"/>

<h3><@spring.message "subject"/> ${subject.subjectCode!""}</h3>

<form method="post" action="subject/update.act">
    <table>
        <tr>
            <td><@spring.message "code"/>:</td>
            <td>
                <input type="text" name="subjectCode" value="${subject.subjectCode!""}">
            </td>
        </tr>
        <tr>
            <td><@spring.message "name"/>:</td>
            <td>
                <input type="text" name="subjectName" value="${subject.subjectName!""}">
            </td>
        </tr>
        <tr>
            <td><@spring.message "hours"/>:</td>
            <td>
                <input type="text" name="hours" value="${subject.hours!""}">
            </td>
        </tr>
        <tr>
            <td><@spring.message "controlPoints"/>:</td>
            <td>
                <input type="text" name="controlPoints" value="${subject.controlPoints!""}">
            </td>
        </tr>
        <tr>
            <td><@spring.message "finalControl"/>:</td>
            <td>
                <select name="finalControl">
                    <option value="0"><@spring.message "select"/></option>
                    <option <#if subject.finalControl?? && subject.finalControl == 1>selected</#if> value="1"><@spring.message "noExam"/></option>
                    <option <#if subject.finalControl?? && subject.finalControl == 2>selected</#if> value="2"><@spring.message "exam"/></option>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "teachers"/>:</td>
            <td>
            <#if subjectTeachers??>
                <#list subjectTeachers as teacher>
                    <div id="teacher${teacher_index + 1}">
                    ${teacher_index + 1}
                        <a href="teacher.act?teacherId=${teacher.teacherId!"0"}">${teacher.shortName!""}</a>
                        <input type="button" value="x"
                               onclick="deleteSubjectTeacher('teacher${teacher_index + 1}', '${teacher.teacherId!"0"}', '${subject.subjectId!"0"}');">
                        <input type="hidden" name="teacherId" value="${teacher.teacherId!"0"}">
                    </div>
                </#list>
            </#if>
            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" value="<@spring.message "update"/>"/>
            </td>
        </tr>
    </table>

    <input type="hidden" name="subjectId" value="${subject.subjectId!"0"}">
</form>

<@footer "none"/>