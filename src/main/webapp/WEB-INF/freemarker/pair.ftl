<#-- @ftlvariable name="pair" type="com.coffeebreak.schedule.entity.Pair" -->
<#-- @ftlvariable name="days" type="com.coffeebreak.schedule.enums.PairDay[]" -->
<#-- @ftlvariable name="types" type="com.coffeebreak.schedule.enums.PairType[]" -->
<#-- @ftlvariable name="subjects" type="java.util.List<com.coffeebreak.schedule.entity.Subject>" -->
<#-- @ftlvariable name="pairGroups" type="java.util.List<com.coffeebreak.schedule.entity.Group>" -->
<#-- @ftlvariable name="subjectTeachers" type="java.util.List<com.coffeebreak.schedule.entity.Teacher>" -->

<#include "macros/general.ftl"/>

<@header "pair"/>

<h3><@spring.message "pair"/></h3>

<form method="post" action="pair/update.act">
    <table>
        <tr>
            <td><@spring.message "day"/>:</td>
            <td>
                <select name="day">
                    <option value="0"><@spring.message "select"/></option>
                <#list days as day>
                    <option <@selectedValue pair.day day.code/>
                            value="${day.code!"0"}"><@spring.message "${day.description}"/></option>
                </#list>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "number"/>:</td>
            <td>
                <select name="number">
                    <option value="0"><@spring.message "select"/></option>
                <#assign numbers=6>
                <#list 1..numbers as number>
                    <option <@selectedValue pair.number number/> value="${number}">${number}</option>
                </#list>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "room"/>:</td>
            <td><input type="text" name="room" value="${pair.room!""}"></td>
        </tr>
        <tr>
            <td><@spring.message "week"/>:</td>
            <td>
                <select name="week">
                    <option value="0"><@spring.message "select"/></option>
                <#assign weeks=2>
                <#list 1..weeks as week>
                    <option <@selectedValue pair.week week/> value="${week}">${week}</option>
                </#list>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "type"/>:</td>
            <td>
                <select name="type">
                    <option value="0"><@spring.message "select"/></option>
                <#list types as type>
                    <option <@selectedValue pair.type type.code/>
                            value="${type.code!"0"}"><@spring.message "${type.description}"/></option>
                </#list>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "subject"/>:</td>
            <td>
            <#if subjects??>
                <select id="subjectId" name="subjectId" onchange="showDropDownTeachers('pair');">
                    <option value="0"><@spring.message "select"/></option>
                    <#list subjects?sort_by("subjectName") as subject>
                        <option <@selectedValue subject.subjectId pair.subject.subjectId/>
                                value="${subject.subjectId!"0"}">${subject.subjectCode!""}</option>
                    </#list>
                </select>
            </#if>
            </td>
        </tr>
        <tr>
            <td><@spring.message "teacher"/></td>
            <td>
            <#if subjectTeachers??>
                <select id="teacherId" name="teacherId">
                    <option value="0"><@spring.message "select"/></option>
                    <#list subjectTeachers?sort_by("shortName") as teacher>
                        <option <@selectedValue teacher.teacherId pair.teacher.teacherId/>
                                value="${teacher.teacherId!"0"}">${teacher.shortName!""}</option>
                    </#list>
                </select>
            </#if>
            </td>
        </tr>
        <tr>
            <td><@spring.message "groups"/>:</td>
            <td>
            <#if pairGroups??>
                <#list pairGroups as group>
                    <div id="group${group_index + 1}">
                    ${group_index + 1}
                        <a href="group.act?groupId=${group.groupId!"0"}">${group.groupCode!""}</a>
                        <input type="button" value="x"
                               onclick="deletePairGroup('group${group_index + 1}', '${group.groupId!"0"}', '${pair.pairId!"0"}');">
                        <input type="hidden" name="groupId" value="${group.groupId!"0"}">
                    </div>
                </#list>
            </#if>
            </td>
        </tr>
        <tr>
            <td><input type="submit" value="<@spring.message "update"/>"></td>
        </tr>
    </table>
    <input type="hidden" name="pairId" value="${pair.pairId!"0"}">
</form>

<@footer "none"/>