<#-- @ftlvariable name="faculty" type="com.coffeebreak.schedule.entity.Faculty" -->
<#-- @ftlvariable name="faculties" type="java.util.List<com.coffeebreak.schedule.entity.Faculty>" -->

<#include "macros/general.ftl"/>

<@header "faculties"/>

<form method="post" action="faculty/add.act">
    <table>
        <tr>
            <td><@spring.message "code"/></td>
            <td>
                <input type="text" name="facultyCode">
            </td>
        </tr>
        <tr>
            <td><@spring.message "name"/></td>
            <td>
                <input type="text" name="facultyName">
            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" value="<@spring.message "add"/>"/>
            </td>
        </tr>
    </table>
</form>

<h3><@spring.message "listFaculties"/></h3>
<#if faculties??>
<table>
    <tr>
        <th>#</th>
        <th><@spring.message "code"/></th>
        <th><@spring.message "name"/></th>
        <th>x</th>
    </tr>
    <#list faculties as faculty>
        <tr>
            <td>${faculty_index + 1}</td>
            <td><a href="faculty.act?facultyId=${faculty.facultyId!"0"}">${faculty.facultyCode!""}</a></td>
            <td>${faculty.facultyName!""}</td>
            <td>
                <input type="button" value="x" onclick="deleteEntity('faculty', '${faculty.facultyId!"0"}');">
            </td>
        </tr>
    </#list>
</table>
</#if>

<@footer "faculties"/>