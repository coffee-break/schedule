<#-- @ftlvariable name="faculty" type="com.coffeebreak.schedule.entity.Faculty" -->
<#-- @ftlvariable name="faculties" type="java.util.List<com.coffeebreak.schedule.entity.Faculty>" -->
<#-- @ftlvariable name="department" type="com.coffeebreak.schedule.entity.Department" -->
<#-- @ftlvariable name="departments" type="java.util.List<com.coffeebreak.schedule.entity.Department>" -->

<#include "macros/general.ftl"/>

<@header "departments"/>

<form method="post" action="department/add.act">
    <table>
        <tr>
            <td><@spring.message "code"/></td>
            <td>
                <input type="text" name="departmentCode">
            </td>
        </tr>
        <tr>
            <td><@spring.message "name"/></td>
            <td>
                <input type="text" name="departmentName">
            </td>
        </tr>
        <tr>
            <td><@spring.message "faculty"/></td>
            <td>
                <select name="facultyId">
                <#if faculties??>
                    <option value="0"><@spring.message "select"/></option>
                    <#list faculties?sort_by("facultyCode") as faculty>
                        <option value="${faculty.facultyId}">${faculty.facultyCode}</option>
                    </#list>
                </#if>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" value="<@spring.message "add"/>"/>
            </td>
        </tr>
    </table>
</form>

<h3><@spring.message "listDepartments"/></h3>
<#if departments??>
<table>
    <tr>
        <th>#</th>
        <th><@spring.message "code"/></th>
        <th><@spring.message "name"/></th>
        <th><@spring.message "faculty"/></th>
        <th>x</th>
    </tr>
    <#list departments as department>
        <tr>
            <td>${department_index + 1}</td>
            <td>
                <a href="department.act?departmentId=${department.departmentId!"0"}">${department.departmentCode!""}</a>
                <br>
            </td>
            <td>${department.departmentName!""}</td>
            <td>
                <a href="faculty.act?facultyId=${department.faculty.facultyId!"0"}">${department.faculty.facultyCode!""}</a>
            </td>
            <td>
                <input type="button" value="x" onclick="deleteEntity('department', '${department.departmentId!"0"}');">
            </td>
        </tr>
    </#list>
</table>
</#if>

<@footer "departments"/>