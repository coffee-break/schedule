<#-- @ftlvariable name="pairs" type="java.util.List<com.coffeebreak.schedule.entity.Pair>" -->
<#-- @ftlvariable name="scheduleDay" type="java.lang.String" -->
<#-- @ftlvariable name="scheduleWeek" type="java.lang.Integer" -->
<#-- @ftlvariable name="pairTypes" type="com.coffeebreak.schedule.enums.PairType[]" -->

<div class="dailySchedule">
    <div class="day"><@spring.message "${scheduleDay}"/> (<@spring.message "week"/> - ${scheduleWeek})</div>
    <table>
    <#if pairs??>
        <#list pairs?sort_by("number") as pair>
            <tr>
                <td>${pair.number!""}</td>
                <td><a href="subject.act?subjectId=${pair.subject.subjectId!"0"}">${pair.subject.subjectCode!""}</a></td>
                <td>
                    <#list pairTypes as type>
                    <#if type.code == pair.type>
                    <@spring.message "${type.description}"/>
                    </#if>
                    </#list>
                </td>
                <td><a href="teacher.act?teacherId=${pair.teacher.teacherId!"0"}">${pair.teacher.shortName!""}</a></td>
                <td>${pair.room!""}</td>
            </tr>
        </#list>
    </#if>
    </table>
</div>