<#-- @ftlvariable name="scheduleType" type="java.lang.Integer" -->

<#include "../macros/general.ftl"/>

<@header "schedule"/>

<#if scheduleType == 1>
    <#include "daily.ftl"/>
<#elseif scheduleType == 2>
    <#include "weekly.ftl"/>
</#if>

<@footer "schedule"/>