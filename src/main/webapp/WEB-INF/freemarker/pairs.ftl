<#-- @ftlvariable name="types" type="com.coffeebreak.schedule.enums.PairType[]" -->
<#-- @ftlvariable name="days" type="com.coffeebreak.schedule.enums.PairDay[]" -->
<#-- @ftlvariable name="pairs" type="java.util.List<com.coffeebreak.schedule.entity.Pair>" -->
<#-- @ftlvariable name="subjects" type="java.util.List<com.coffeebreak.schedule.entity.Subject>" -->
<#-- @ftlvariable name="groups" type="java.util.List<com.coffeebreak.schedule.entity.Group>" -->

<#include "macros/general.ftl"/>

<@header "pairs"/>

<form method="post" action="pair/add.act">
    <table>
        <tr>
            <td><@spring.message "day"/></td>
            <td>
                <select name="day">
                    <option value="0"><@spring.message "select"/></option>
                <#list days as day>
                    <option value="${day.code!"0"}"><@spring.message "${day.description}"/></option>
                </#list>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "number"/></td>
            <td>
                <select name="number">
                    <option value="0"><@spring.message "select"/></option>
                <#assign numbers=6>
                <#list 1..numbers as number>
                    <option value="${number}">${number}</option>
                </#list>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "room"/></td>
            <td>
                <input type="text" name="room">
            </td>
        </tr>
        <tr>
            <td><@spring.message "week"/></td>
            <td>
                <select name="week">
                    <option value="0"><@spring.message "select"/></option>
                <#assign weeks=2>
                <#list 1..weeks as week>
                    <option value="${week}">${week}</option>
                </#list>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "type"/></td>
            <td>
                <select name="type">
                    <option value="0"><@spring.message "select"/></option>
                <#list types as type>
                    <option value="${type.code!"0"}"><@spring.message "${type.description}"/></option>
                </#list>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "subject"/></td>
            <td>
                <select id="subjectId" name="subjectId" onchange="showDropDownTeachers('pair');">
                <#if subjects??>
                    <option value="0"><@spring.message "select"/></option>
                    <#list subjects as subject>
                        <option value="${subject.subjectId}">${subject.subjectCode}</option>
                    </#list>
                </#if>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "teacher"/></td>
            <td>
                <select id="teacherId" name="teacherId">
                    <option value="0"><@spring.message "select"/></option>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "faculty"/></td>
            <td>
                <select id="facultyId" onchange="showDropDownDepartments('pair');">
                <#if faculties??>
                    <option value="0"><@spring.message "select"/></option>
                    <#list faculties?sort_by("facultyCode") as faculty>
                        <option value="${faculty.facultyId}">${faculty.facultyCode}</option>
                    </#list>
                </#if>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "department"/></td>
            <td>
                <select id="departmentId" name="departmentId">
                    <option value="0"><@spring.message "select"/></option>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "group"/></td>
            <td>
                <div id="checkBoxGroups"></div>
            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" value="<@spring.message "add"/>"/>
            </td>
        </tr>
    </table>
</form>

<h3><@spring.message "listPairs"/></h3>
<#if pairs??>
<table>
    <tr>
        <th>#</th>
        <th><@spring.message "day"/></th>
        <th><@spring.message "number"/></th>
        <th><@spring.message "room"/></th>
        <th><@spring.message "week"/></th>
        <th><@spring.message "type"/></th>
        <th><@spring.message "subject"/></th>
        <th><@spring.message "teacher"/></th>
        <th>x</th>
    </tr>
    <#list pairs as pair>
        <tr>
            <td>
                <a href="pair.act?pairId=${pair.pairId!"0"}">${pair_index + 1}</a>
            </td>
            <td>
                <#list days as day>
                    <#if pair.day == day.code><@spring.message "${day.description}"/></#if>
                </#list>
            </td>
            <td>${pair.number!""}</td>
            <td>${pair.room!""}</td>
            <td>${pair.week!""}</td>
            <td>
                <#list types as type>
                    <#if pair.type == type.code><@spring.message "${type.description}"/></#if>
                </#list>
            </td>
            <td><a href="subject.act?subjectId=${pair.subject.subjectId!""}">${pair.subject.subjectCode!""}</a></td>
            <td><a href="teacher.act?teacherId=${pair.teacher.teacherId!""}">${pair.teacher.shortName!""}</a></td>
            <td>
                <input type="button" value="x" onclick="deleteEntity('pair', '${pair.pairId!"0"}');">
            </td>
        </tr>
    </#list>
</table>
</#if>

<@footer "pairs"/>