<#-- @ftlvariable name="teacher" type="com.coffeebreak.schedule.entity.Teacher" -->
<#-- @ftlvariable name="faculties" type="java.util.List<com.coffeebreak.schedule.entity.Faculty>" -->
<#-- @ftlvariable name="facultyDepartments" type="java.util.List<com.coffeebreak.schedule.entity.Department>" -->
<#-- @ftlvariable name="teacherSubjects" type="java.util.List<com.coffeebreak.schedule.entity.Subject>" -->

<#include "macros/general.ftl"/>

<@header "teacher"/>

<h3><@spring.message "teacher"/> ${teacher.shortName!""}</h3>

<form method="post" action="teacher/update.act">
    <table>
        <tr>
            <td>Short name:</td>
            <td>
                <input type="text" name="shortName" value="${teacher.shortName!""}">
            </td>
        </tr>
        <tr>
            <td>First name:</td>
            <td>
                <input type="text" name="firstName" value="${teacher.firstName!""}">
            </td>
        </tr>
        <tr>
            <td>Last name:</td>
            <td>
                <input type="text" name="lastName" value="${teacher.lastName!""}">
            </td>
        </tr>
        <tr>
            <td>Middle name:</td>
            <td>
                <input type="text" name="middleName" value="${teacher.middleName!""}">
            </td>
        </tr>
        <tr>
            <td><@spring.message "faculty"/>:</td>
            <td>
                <select id="facultyId" onchange="showDropDownDepartments('teacher');">
                    <option value="0"><@spring.message "select"/></option>
                <#if faculties??>
                    <#list faculties?sort_by("facultyCode") as faculty>
                        <option <@selectedValue faculty.facultyId teacher.department.faculty.facultyId/>
                                value="${faculty.facultyId!"0"}">${faculty.facultyCode!""}</option>
                    </#list>
                </#if>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "department"/>:</td>
            <td>
                <select id="departmentId" name="departmentId">
                    <option value="0"><@spring.message "select"/></option>
                <#if facultyDepartments??>
                    <#list facultyDepartments?sort_by("departmentCode") as department>
                        <option <@selectedValue department.departmentId teacher.department.departmentId/>
                                value="${department.departmentId!"0"}">${department.departmentCode!""}</option>
                    </#list>
                </#if>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "subjects"/>:</td>
            <td>
            <#if teacherSubjects??>
                <#list teacherSubjects as subject>
                    <div id="subject${subject_index + 1}">
                    ${subject_index + 1}
                        <a href="subject.act?subjectId=${subject.subjectId!"0"}">${subject.subjectCode!""}</a>
                        <input type="button" value="x"
                               onclick="deleteTeacherSubject('subject${subject_index + 1}', '${subject.subjectId!"0"}', '${teacher.teacherId!"0"}');">
                        <input type="hidden" name="subjectId" value="${subject.subjectId!"0"}">
                    </div>
                </#list>
            </#if>
            </td>
        </tr>
        <tr>
            <td><input type="submit" value="<@spring.message "update"/>"></td>
        </tr>
    </table>

    <input type="hidden" name="teacherId" value="${teacher.teacherId!"0"}">
</form>

<@footer "none"/>