<#-- @ftlvariable name="department" type="com.coffeebreak.schedule.entity.Department" -->
<#-- @ftlvariable name="faculties" type="java.util.List<com.coffeebreak.schedule.entity.Faculty>" -->

<#include "macros/general.ftl"/>

<@header "department"/>

<h3><@spring.message "department"/> ${department.departmentCode!""}</h3>

<form method="post" action="department/update.act">
    <table>
        <tr>
            <td><@spring.message "code"/>:</td>
            <td>
                <input type="text" name="departmentCode" value="${department.departmentCode!""}">
            </td>
        </tr>
        <tr>
            <td><@spring.message "name"/>:</td>
            <td>
                <input type="text" name="departmentName" value="${department.departmentName!""}">
            </td>
        </tr>
        <tr>
            <td><@spring.message "faculty"/>:</td>
            <td>
            <#if faculties??>
                <select name="facultyId">
                    <option value="0"><@spring.message "select"/></option>
                    <#list faculties?sort_by("facultyCode") as faculty>
                        <option <@selectedValue faculty.facultyId department.faculty.facultyId/>
                                value="${faculty.facultyId!"0"}">${faculty.facultyCode!""}</option>
                    </#list>
                </select>
            </#if>
            </td>
        </tr>
        <tr>
            <td><input type="submit" value="<@spring.message "update"/>"></td>
        </tr>
    </table>

    <input type="hidden" name="departmentId" value="${department.departmentId!"0"}">
</form>

<@footer "none"/>