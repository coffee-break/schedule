<#import "/spring.ftl" as spring/>

<#-- Header begin -->
<#macro header title>

<!DOCTYPE html>
<html>
<head>

    <title><@spring.message "${title}"/></title>

    <link rel="stylesheet" href="resources/css/general.css">
    <script type="text/javascript" src="resources/js/general.js"></script>
    <script type="text/javascript" src="resources/js/jquery/jquery-1.11.0.min.js"></script>

    <#include "ajax.ftl"/>

</head>

<body>

<div class="wrapper">
    <header class="header">
        <h1><@spring.message "schedule"/></h1>
    </header><!-- .header -->

    <@middle title/>

</#macro>
<#-- Header end -->

<#-- Middle begin -->
<#macro middle title>

    <div class="middle">
        <div class="container">
            <main class="content">
                <div><h2><@spring.message "${title}"/></h2></div>

</#macro>
<#-- Middle end -->

<#-- Sidebar begin -->
<#macro sidebar page>

        <aside class="sidebar">
            <ul>
                <li <#if page = "schedule">class="active"</#if>><a href="dashboard.act"><@spring.message "schedule"/></a></li>
                <li <#if page = "faculties">class="active"</#if>><a href="faculties.act"><@spring.message "faculties"/></a></li>
                <li <#if page = "departments">class="active"</#if>><a href="departments.act"><@spring.message "departments"/></a></li>
                <li <#if page = "teachers">class="active"</#if>><a href="teachers.act"><@spring.message "teachers"/></a></li>
                <li <#if page = "groups">class="active"</#if>><a href="groups.act"><@spring.message "groups"/></a></li>
                <li <#if page = "subjects">class="active"</#if>><a href="subjects.act"><@spring.message "subjects"/></a></li>
                <li <#if page = "pairs">class="active"</#if>><a href="pairs.act"><@spring.message "pairs"/></a></li>
            </ul>
        </aside><!-- .sidebar -->

</#macro>
<#-- Sidebar end -->

<#-- Footer begin -->
<#macro footer page>
            </main><!-- .content -->
        </div><!-- .container -->
        <@sidebar page/>
    </div><!-- .middle -->
</div><!-- .wrapper -->

<footer class="footer">&copy; 2014, Coffee Break</footer>

</body>
</html>

</#macro>
<#-- Footer end -->

<#-- Selected begin -->
<#macro selectedValue firstValue secondValue>
    <#if firstValue == secondValue>
    selected="selected"
    </#if>
</#macro>
<#-- Selected end -->