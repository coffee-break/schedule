<script type="text/javascript">
    function showDropDownDepartments(entityName) {
        var facultyOption = $('#facultyId option:selected');
        if (facultyOption.val() != "0") {
            var parameters = 'facultyId=' + facultyOption.val();
        <@ajax action="showDropDownDepartments.act">
            $('#departmentId').html(data);
            if (entityName == 'subject') {
                $('#departmentId').attr('onchange', 'showCheckBoxTeachers();');
            }
            if (entityName == 'pair') {
                $('#departmentId').attr('onchange', 'showCheckBoxGroups();');
            }
            if (entityName == 'schedule') {
                $('#departmentId').attr('onchange', 'showDropDownGroups();');
            }
        </@ajax>
        }
    }
    function showCheckBoxTeachers() {
        var departmentOption = $('#departmentId option:selected');
        if (departmentOption.val() != "0") {
            var parameters = 'departmentId=' + departmentOption.val();
        <@ajax action="showCheckBoxTeachers.act">
            $('#checkBoxTeachers').html(data);
        </@ajax>
        }
    }
    function showCheckBoxGroups() {
        var departmentOption = $('#departmentId option:selected');
        if (departmentOption.val() != "0") {
            var parameters = 'departmentId=' + departmentOption.val();
        <@ajax action="showCheckBoxGroups.act">
            $('#checkBoxGroups').html(data);
        </@ajax>
        }
    }
    function showDropDownGroups(entityName) {
        var departmentOption = $('#departmentId option:selected');
        if (departmentOption.val() != "0") {
            var parameters = 'departmentId=' + departmentOption.val();
        <@ajax action="showDropDownGroups.act">
            $('#groupId').html(data);
        </@ajax>
        }
    }
    function showDropDownTeachers() {
        var subjectOption = $('#subjectId option:selected');
        if (subjectOption.val() != "0") {
            var parameters = 'subjectId=' + subjectOption.val();
        <@ajax action="showDropDownTeachers.act">
            $('#teacherId').html(data);
        </@ajax>
        }
    }
    function deleteFacultyDepartment(divId, departmentId) {
        var parameters = 'departmentId=' + departmentId;
        if (confirm("Are you sure you want to delete this department?")) {
            if (departmentId != "") {
            <@ajax action="deleteFacultyDepartment.act">
                if (data == "ok") {
                    $('#' + divId).remove();
                }
            </@>
            }
        }
    }
    function deleteTeacherSubject(divId, subjectId, teacherId) {
        var parameters = 'subjectId=' + subjectId + '&teacherId=' + teacherId;
        if (confirm("Are you sure you want to delete this subject?")) {
            if (subjectId != "") {
            <@ajax action="deleteTeacherSubject.act">
                if (data == "ok") {
                    $('#' + divId).remove();
                }
            </@>
            }
        }
    }
    function deleteSubjectTeacher(divId, teacherId, subjectId) {
        var parameters = 'teacherId=' + teacherId + '&subjectId=' + subjectId;
        if (confirm("Are you sure you want to delete this teacher?")) {
            if (teacherId != "") {
            <@ajax action="deleteSubjectTeacher.act">
                if (data == "ok") {
                    $('#' + divId).remove();
                }
            </@>
            }
        }
    }
    function deleteGroupPair(trId, pairId, groupId) {
        var parameters = 'pairId=' + pairId + '&groupId=' + groupId;
        if (confirm("Are you sure you want to delete this pair?")) {
            if (pairId != "") {
            <@ajax action="deleteGroupPair.act">
                if (data == "ok") {
                    $('#' + trId).remove();
                }
            </@>
            }
        }
    }
    function deletePairGroup(divId, groupId, pairId) {
        var parameters = 'groupId=' + groupId + '&pairId=' + pairId;
        if (confirm("Are you sure you want to delete this group?")) {
            if (groupId != "") {
            <@ajax action="deletePairGroup.act">
                if (data == "ok") {
                    $('#' + divId).remove();
                }
            </@>
            }
        }
    }

    <#macro ajax action>
    $.ajax({
        url: '${action}',
        type: 'POST',
    <#--parameters need to be set before the macro!-->
        data: parameters,
        dataType: 'text',
        success: function (data) {
            <#nested>
        },
        error: function (xhr) {
            if ((xhr.status == 401) || (xhr.status == 500)) {
                // unauthorized
                location.replace('""');
            } else {
                alert('Handle AJAX request failed! Status is: ' + xhr.status);
            }
        }
    });
    </#macro>
</script>