<#-- @ftlvariable name="faculties" type="java.util.List<com.coffeebreak.schedule.entity.Faculty>" -->
<#-- @ftlvariable name="groups" type="java.util.List<com.coffeebreak.schedule.entity.Group>" -->

<#include "macros/general.ftl"/>

<@header "groups"/>

<form method="post" action="group/add.act">
    <table>
        <tr>
            <td><@spring.message "code"/></td>
            <td>
                <input type="text" name="groupCode">
            </td>
        </tr>
        <tr>
            <td><@spring.message "course"/></td>
            <td>
                <select name="course">
                    <option value="0"><@spring.message "select"/></option>
                <#assign courses=6>
                <#list 1..courses as course>
                    <option value="${course}">${course}</option>
                </#list>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "faculty"/></td>
            <td>
                <select id="facultyId" onchange="showDropDownDepartments('group');">
                <#if faculties??>
                    <option value="0"><@spring.message "select"/></option>
                    <#list faculties?sort_by("facultyCode") as faculty>
                        <option value="${faculty.facultyId}">${faculty.facultyCode}</option>
                    </#list>
                </#if>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "department"/></td>
            <td>
                <select id="departmentId" name="departmentId">
                    <option value="0"><@spring.message "select"/></option>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" value="<@spring.message "add"/>"/>
            </td>
        </tr>
    </table>
</form>

<h3><@spring.message "listGroups"/></h3>
<#if groups??>
<table>
    <tr>
        <th>#</th>
        <th><@spring.message "code"/></th>
        <th><@spring.message "course"/></th>
        <th><@spring.message "department"/></th>
        <th>x</th>
    </tr>
    <#list groups as group>
        <tr>
            <td>${group_index + 1}</td>
            <td>
                <a href="group.act?groupId=${group.groupId!"0"}">${group.groupCode!""}</a>
            </td>
            <td>${group.course!""}</td>
            <td>
                <a href="department.act?departmentId=${group.department.departmentId!"0"}">${group.department.departmentCode!""}</a>
            </td>
            <td>
                <input type="button" value="x" onclick="deleteEntity('group', '${group.groupId!"0"}');">
            </td>
        </tr>
    </#list>
</table>
</#if>

<@footer "groups"/>