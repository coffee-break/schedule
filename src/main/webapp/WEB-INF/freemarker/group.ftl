<#-- @ftlvariable name="faculties" type="java.util.List<com.coffeebreak.schedule.entity.Faculty>" -->
<#-- @ftlvariable name="groupPairs" type="java.util.List<com.coffeebreak.schedule.entity.Pair>" -->
<#-- @ftlvariable name="group" type="com.coffeebreak.schedule.entity.Group" -->
<#-- @ftlvariable name="days" type="com.coffeebreak.schedule.enums.PairDay[]" -->
<#-- @ftlvariable name="types" type="com.coffeebreak.schedule.enums.PairType[]" -->
<#-- @ftlvariable name="facultyDepartments" type="java.util.List<com.coffeebreak.schedule.entity.Department>" -->

<#include "macros/general.ftl"/>

<@header "group"/>

<h3><@spring.message "group"/> ${group.groupCode!""}</h3>

<form method="post" action="group/update.act">
    <table>
        <tr>
            <td><@spring.message "code"/>:</td>
            <td>
                <input type="text" name="groupCode" value="${group.groupCode!""}">
            </td>
        </tr>
        <tr>
            <td><@spring.message "course"/>:</td>
            <td>
                <select name="course">
                    <option value="0"><@spring.message "select"/></option>
                <#assign courses=6>
                <#list 1..courses as course>
                    <option <@selectedValue group.course course/> value="${course}">${course}</option>
                </#list>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "faculty"/>:</td>
            <td>
                <select id="facultyId" onchange="showDropDownDepartments('group');">
                    <option value="0"><@spring.message "select"/></option>
                <#if faculties??>
                    <#list faculties?sort_by("facultyCode") as faculty>
                        <option <@selectedValue faculty.facultyId group.department.faculty.facultyId/>
                                value="${faculty.facultyId!"0"}">${faculty.facultyCode!""}</option>
                    </#list>
                </#if>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "department"/>:</td>
            <td>
                <select id="departmentId" name="departmentId">
                    <option value="0"><@spring.message "select"/></option>
                <#if facultyDepartments??>
                    <#list facultyDepartments?sort_by("departmentCode") as department>
                        <option <@selectedValue department.departmentId group.department.departmentId/>
                                value="${department.departmentId!"0"}">${department.departmentCode!""}</option>
                    </#list>
                </#if>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "pairs"/>:</td>
            <td>
            <#if groupPairs??>
                <table>
                    <tr>
                        <th>#</th>
                        <th><@spring.message "subject"/></th>
                        <th><@spring.message "type"/></th>
                        <th><@spring.message "number"/></th>
                        <th><@spring.message "day"/></th>
                        <th><@spring.message "week"/></th>
                        <th>x</th>
                    </tr>
                <#list groupPairs as pair>
                    <tr id="pair${pair_index + 1}">
                        <td>
                            <a href="pair.act?pairId=${pair.pairId!"0"}">${pair_index + 1}</a>
                        </td>
                        <td>
                            <a href="subject.act?subjectId=${pair.subject.subjectId!""}">${pair.subject.subjectCode!""}</a>
                        </td>
                        <td>
                        <#list types as type>
                            <#if pair.type == type.code><@spring.message "${type.description}"/></#if>
                        </#list>
                        </td>
                        <td>${pair.number!""}</td>
                        <td>
                        <#list days as day>
                            <#if pair.day == day.code><@spring.message "${day.description}"/></#if>
                        </#list>
                        </td>
                        <td>${pair.week!""}</td>
                        <td>
                            <input type="button" value="x"
                                   onclick="deleteGroupPair('pair${pair_index + 1}', '${pair.pairId!"0"}', '${group.groupId!"0"}');">
                        </td>
                        <input type="hidden" name="pairId" value="${pair.pairId!"0"}">
                    </tr>
                </#list>
                </table>
            </#if>
            </td>
        </tr>
        <tr>
            <td><input type="submit" value="<@spring.message "update"/>"></td>
        </tr>
    </table>

    <input type="hidden" name="groupId" value="${group.groupId!"0"}">
</form>

<@footer "none"/>