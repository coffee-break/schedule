<#-- @ftlvariable name="faculties" type="java.util.List<com.coffeebreak.schedule.entity.Faculty>" -->
<#-- @ftlvariable name="days" type="com.coffeebreak.schedule.enums.PairDay[]" -->
<#-- @ftlvariable name="currentWeek" type="java.lang.Integer" -->
<#-- @ftlvariable name="currentDay" type="java.lang.Integer" -->

<#include "macros/general.ftl"/>

<@header "schedule"/>

<form method="post" action="schedule.act">
    <table>
        <tr>
            <td><@spring.message "faculty"/></td>
            <td>
                <select id="facultyId" onchange="showDropDownDepartments('schedule');">
                <#if faculties??>
                    <option value="0"><@spring.message "select"/></option>
                    <#list faculties?sort_by("facultyCode") as faculty>
                        <option value="${faculty.facultyId}">${faculty.facultyCode}</option>
                    </#list>
                </#if>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "department"/></td>
            <td>
                <select id="departmentId" name="departmentId">
                    <option value="0"><@spring.message "select"/></option>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "group"/></td>
            <td>
                <select id="groupId" name="groupId">
                    <option value="0"><@spring.message "select"/></option>
                </select>
            </td>
        </tr>
        <tr>
            <td><@spring.message "type"/></td>
            <td>
                <select id="scheduleType" name="scheduleType" onchange="showDropDownPeriod();">
                    <option value="0"><@spring.message "select"/></option>
                    <option value="1"><@spring.message "daily"/></option>
                    <option value="2"><@spring.message "weekly"/></option>
                </select>
            </td>
        </tr>
        <tr id="trScheduleDay" hidden="hidden">
            <td><@spring.message "day"/></td>
            <td>
                <select name="scheduleDay">
                    <option value="0"><@spring.message "select"/></option>
                <#list days as day>
                    <option <@selectedValue currentDay day.code/>
                            value="${day.code!"0"}"><@spring.message "${day.description}"/></option>
                </#list>
                </select>
            </td>
        </tr>
        <tr id="trScheduleWeek" hidden="hidden">
            <td><@spring.message "week"/></td>
            <td>
                <select name="scheduleWeek">
                    <option value="0"><@spring.message "select"/></option>
                <#assign weeks=2>
                <#list 1..weeks as week>
                    <option <@selectedValue currentWeek week/> value="${week}">${week}</option>
                </#list>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" value="<@spring.message "show"/>"/>
            </td>
        </tr>
    </table>
</form>

<@footer "schedule"/>