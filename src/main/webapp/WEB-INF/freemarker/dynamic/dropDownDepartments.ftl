<#-- @ftlvariable name="departments" type="java.util.List<com.coffeebreak.schedule.entity.Department>" -->

<#import "/spring.ftl" as spring/>

<#if departments??>
    <option value="0"><@spring.message "select"/></option>
    <#list departments?sort_by("departmentCode") as department>
        <option value="${department.departmentId}">${department.departmentCode}</option>
    </#list>
</#if>