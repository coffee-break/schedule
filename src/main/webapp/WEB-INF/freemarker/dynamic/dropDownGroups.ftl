<#-- @ftlvariable name="groups" type="java.util.List<com.coffeebreak.schedule.entity.Group>" -->

<#import "/spring.ftl" as spring/>

<#if groups??>
    <option value="0"><@spring.message "select"/></option>
    <#list groups?sort_by("groupCode") as group>
        <option value="${group.groupId}">${group.groupCode}</option>
    </#list>
</#if>