<#-- @ftlvariable name="teachers" type="java.util.List<com.coffeebreak.schedule.entity.Teacher>" -->

<#import "/spring.ftl" as spring/>

<#if teachers??>
    <option value="0"><@spring.message "select"/></option>
    <#list teachers?sort_by("shortName") as teacher>
        <option value="${teacher.teacherId}">${teacher.shortName}</option>
    </#list>
</#if>