<#-- @ftlvariable name="groups" type="java.util.List<com.coffeebreak.schedule.entity.Group>" -->

<#if groups??>
    <#list groups?sort_by("groupCode") as group>
        ${group_index + 1}
        ${group.groupCode!""}
        <input type="checkbox" name="groupId" value="${group.groupId!"0"}">
        <br>
    </#list>
</#if>