<#-- @ftlvariable name="teachers" type="java.util.List<com.coffeebreak.schedule.entity.Teacher>" -->

<#if teachers??>
    <#list teachers?sort_by("shortName") as teacher>
        ${teacher_index + 1}
        ${teacher.shortName!""}
        <input type="checkbox" name="teacherId" value="${teacher.teacherId!"0"}">
        <br>
    </#list>
</#if>