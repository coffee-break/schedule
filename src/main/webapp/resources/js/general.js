function deleteEntity(entityName, entityId) {
    if (confirm("Are you sure you want to delete this " + entityName + "?")) {
        var href = entityName + '/delete.act/' + entityId;
        window.open(href, '_self');
    }
}

function showDropDownPeriod() {
    var scheduleType = $('#scheduleType option:selected');
    if (scheduleType.val() == 1) {
        $('#trScheduleDay').show();
        $('#trScheduleWeek').show();
    }
    if (scheduleType.val() == 2) {
        $('#trScheduleWeek').show();
        if ($('#trScheduleDay').is(':visible')) {
            $('#trScheduleDay').hide();
        }
    }
    if (scheduleType.val() == 0) {
        if ($('#trScheduleWeek').is(':visible')) {
            $('#trScheduleWeek').hide();
        }
        if ($('#trScheduleDay').is(':visible')) {
            $('#trScheduleDay').hide();
        }
    }
}